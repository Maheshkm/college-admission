package com.javaproject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class AdmissionDetails {
            StudentAdmission admission=new StudentAdmission();
        	Scanner input=new Scanner(System.in);

            public void admission() throws IOException {
            	System.out.println("------------------------------------------------------------------WELCOME TO STUDENT ADMISSION DETAILS SECTION------------------------------------------------------------------------");
            	System.out.println("ENTER YOUR STUDENT ID :" );
            	String studentId=input.next();
    	        String userpath = "C:\\Users\\" + System.getProperty("user.name") + "\\Desktop\\StudentAdmission\\";
				File check = new File(userpath +studentId+ ".txt");
				if(check.exists()) {
					FileReader file = new FileReader(userpath +studentId+ ".txt");
					BufferedReader buffer =  new BufferedReader(file);
					String line=null;
					while((line =  buffer.readLine())!= null) {
						System.out.println(line);
					}
					buffer.close();
					System.out.println("\n-------------------------------------------------------------------\n");

				}
				else {
					System.out.println("\nStudent Details does not exists.\n\nPlease re-check the entered id\n");
					System.out.println("-------------------------------------------------------------------");
				}
            }		
		public void balanceFeesPayment() throws IOException {
        	System.out.println("ENTER YOUR STUDENT ID :" );
        	String studentId=input.next();
	        String userpath = "C:\\Users\\" + System.getProperty("user.name") + "\\Desktop\\StudentAdmission\\";
			File check = new File(userpath +studentId+ ".txt");
			if(check.exists()) {
			Scanner sc = new Scanner(new File(userpath +studentId+ ".txt"));
			StringBuffer buffer =  new StringBuffer();
			while(sc.hasNextLine()) {
				buffer.append(sc.nextLine()+System.lineSeparator());
			}
			String filecontents = buffer.toString();
			sc.close();
			FileWriter writer =new FileWriter(userpath +studentId+ ".txt");
        	System.out.println("WHICH PAYMENT DO YOU WANT TO CHANGE :\n1.HOSTEL FEES(If Hosteler)\n2.BUS FEES(If Dayscholar)\n3.MESS FEES\n4.TUTION FEES" );
        	int choice = input.nextInt();
        	if(choice == 1) {
            	System.out.println("ENTER AMOUNT FOR HOSTEL FESS YOU ALREADY PAID :" );
            	int amount = input.nextInt();
            	System.out.println("ENTER REMAINING AMOUNT :" );
    			int balance = input.nextInt();
    			int total = amount+ balance;
    			String newamount = String.valueOf(total);
    			String oldline = "HOSTEL FEES PAID : "+amount;
    			String newline = "HOSTEL FEES PAID : "+newamount;
    			String oldline1 = "BALANCE HOSTEL FEES : "+balance;
    			String newline1 = "BALANCE HOSTEL FEES : "+"0";
    			filecontents = filecontents.replaceAll(oldline, newline);
    			filecontents = filecontents.replaceAll(oldline1, newline1);
    			writer.append(filecontents);
            	System.out.println("PAYMENT CHANGES APPLIED. YOU CAN CHECK THE DETAILS IN ADMISSION DETAILS SECTION.");
    			writer.flush();
    			writer.close();

        	}
        	else if(choice == 2) {
            	System.out.println("ENTER AMOUNT FOR BUS FESS YOU ALREADY PAID :" );
            	int amount = input.nextInt();
            	System.out.println("ENTER REMAINING AMOUNT :" );
    			int balance = input.nextInt();
    			int total = amount+ balance;
    			String newamount = String.valueOf(total);
    			String oldline = "BUS FEES PAID : "+amount;
    			String newline = "BUS FEES PAID : "+newamount;
    			String oldline1 = "BALANCE BUS FEES : "+balance;
    			String newline1 = "BALANCE BUS FEES : "+"0";
    			filecontents = filecontents.replaceAll(oldline, newline);
    			filecontents = filecontents.replaceAll(oldline1, newline1);
    			writer.append(filecontents);
            	System.out.println("PAYMENT CHANGES APPLIED. YOU CAN CHECK THE DETAILS IN ADMISSION DETAILS SECTION.");
    			writer.flush();
    			writer.close();

        	}
        	else if(choice == 3) {
            	System.out.println("ENTER AMOUNT FOR MESS FESS YOU ALREADY PAID :" );
            	int amount = input.nextInt();
            	System.out.println("ENTER REMAINING AMOUNT :" );
    			int balance = input.nextInt();
    			int total = amount+ balance;
    			String newamount = String.valueOf(total);
    			String oldline = "MESS FEES PAID : "+amount;
    			String newline = "MESS FEES PAID : "+newamount;
    			String oldline1 = "BALANCE MESS FEES : "+balance;
    			String newline1 = "BALANCE MESS FEES : "+"0";
    			filecontents = filecontents.replaceAll(oldline, newline);
    			filecontents = filecontents.replaceAll(oldline1, newline1);
    			writer.append(filecontents);
            	System.out.println("PAYMENT CHANGES APPLIED. YOU CAN CHECK THE DETAILS IN ADMISSION DETAILS SECTION.");
    			writer.flush();
    			writer.close();

            	
        	}
        	else if(choice == 4) {
            	System.out.println("ENTER AMOUNT FOR TUTION FESS YOU ALREADY PAID :" );
            	int amount = input.nextInt();
            	System.out.println("ENTER REMAINING AMOUNT :" );
    			int balance = input.nextInt();
    			int total = amount+ balance;
    			String newamount = String.valueOf(total);
    			String oldline = "TUTION FEES PAID : "+amount;
    			String newline = "TUTION FEES PAID : "+newamount;
    			String oldline1 = "BALANCE TUTION FEES : "+balance;
    			String newline1 = "BALANCE TUTION FEES : "+"0";
    			filecontents = filecontents.replaceAll(oldline, newline);
    			filecontents = filecontents.replaceAll(oldline1, newline1);
    			writer.append(filecontents);
    			writer.flush();
    			writer.close();
            	System.out.println("PAYMENT CHANGES APPLIED. YOU CAN CHECK THE DETAILS IN ADMISSION DETAILS SECTION.");
        	}
        	else {
        		System.out.println("ENTER A VALID CHOICE!");
    			writer.append(filecontents);
    			writer.flush();
    			writer.close();
        	}
		}
      }	
		
		public void allStudentids() throws IOException {
	        String userpath = "C:\\Users\\" + System.getProperty("user.name") + "\\Desktop\\StudentAdmission\\";
			File check = new File(userpath +"allstudentId"+ ".txt");
			if(check.exists()) {
				FileReader file = new FileReader(userpath +"allstudentId"+ ".txt");
				BufferedReader buffer =  new BufferedReader(file);
				String line=null;
				while((line =  buffer.readLine())!= null) {
					System.out.println(line);
				}
				buffer.close();
				System.out.println("\n-------------------------------------------------------------------\n");

			
		}
    }		
}
