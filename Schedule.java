package com.javaproject;

public class Schedule {
      String day;
      String period1;
      String period2;
      String period3;
      String period4;
      String period5;
      String period6;
      Schedule(String day,String period1,String period2,String period3,String period4,String period5,String period6){
    	  this.day=day;
    	  this.period1=period1;
    	  this.period2=period2;
    	  this.period3=period3;
    	  this.period4=period4;
    	  this.period5=period5;
    	  this.period6=period6;
      }
      
      public String toString() {
    	  return day+"  "+period1+"  "+period2+"  "+period3+"  "+period4+"  "+period5+"  "+period6;
      }
}
