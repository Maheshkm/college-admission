package com.javaproject;

import java.util.*;

import collections.Employee;
//import collections.MySort;

public class StaffAndDepartmentData{
	public void departmentData() {
	    List<StaffDetails> eceStaff=new ArrayList<StaffDetails>();
	    eceStaff.add(new StaffDetails( 25,"N.SIVAMALAN ","HOD           ",40," MR  ","sivamalan1ngec@gmail.com  ","B.E , M.E , PH.D"));
	    eceStaff.add(new StaffDetails( 45,"D.ARUNKUMAR ","PROFESSOR     ",35," MR  ","arunkumarngec@gmail.com   ","B.E , M.E , PH.D"));
	    eceStaff.add(new StaffDetails( 80,"A.POOJA     ","PROFESSOR     ",31," MRS ","poojangec@gmail.com       ","B.E , M.E , PH.D"));
	    eceStaff.add(new StaffDetails( 58,"B.PAUL      ","PROFESSOR     ",40," MR  ","paulngec@gmail.com        ","B.E , M.E , PH.D"));
	    eceStaff.add(new StaffDetails(100,"R.ARJUN     ","ASST.PROFESSOR",30,"MR  ","arjunnge@gmail.com        ","B.E , M.E"));
	    eceStaff.add(new StaffDetails( 83,"A.ABHISHEK  ","ASST.PROFESSOR",33," MR  ","abhishekngec@gmail.com    ","B.E , M.E"));
	    eceStaff.add(new StaffDetails( 67,"H.HARISH    ","ASST.PROFESSOR",31," MR  ","harishngec@gmail.com      ","B.E , M.E"));
	    eceStaff.add(new StaffDetails(180,"R.PRIYA     ","ASST.PROFESSOR",23,"MISS","priyangec@gmail.com       ","B.E , M.E"));
	    eceStaff.add(new StaffDetails( 44,"I.SHANKAR   ","LAB ASSISTANT ",30," MR  ","shankarngec@gmail.com     ","B.E"));
	    eceStaff.add(new StaffDetails(122,"S.ANITHA    ","LAB ASSISTANT ",24,"MISS","anithangec@gmail.com      ","B.E"));
	    
        List <String> eceSubjects=new ArrayList<String >();
        eceSubjects.add("Digital Electronics");
        eceSubjects.add("Signals & Circuit");
        eceSubjects.add("Electronic Circuits");
        eceSubjects.add("DSP");
        eceSubjects.add("VLSI");
        eceSubjects.add("Control Systems");
        
        List <String> eceIv = new ArrayList<String>();
        eceIv.add("All India Radio");
        eceIv.add("AIRTEL");
        eceIv.add("Optical Fiber manufacturing companies");
        eceIv.add("Jio");

        List <String> ecePlacements = new ArrayList<String>();
        ecePlacements.add("AIRTEL");
        ecePlacements.add("HATHWAY");
        ecePlacements.add("HAVELLS ELECTRONICS");
        ecePlacements.add("BOSCH");
        ecePlacements.add("IDEA");

	    List<StaffDetails> cseStaff=new ArrayList<StaffDetails>();
	    cseStaff.add(new StaffDetails(19 ,"P.KEERTHANA","HOD           ",45," MRS ","keerthanangec@gmail.com","B.E , M.E , PH.D"));
	    cseStaff.add(new StaffDetails(30 ,"A.ARAVIND  ","PROFESSOR     ",40," MR  ","aravindngec@gmail.com  ","B.E , M.E , PH.D"));
	    cseStaff.add(new StaffDetails(25 ,"E.VIGNESH  ","PROFESSOR     ",38," MR  ","vigneshngec@gmail.com  ","B.E , M.E , PH.D"));
	    cseStaff.add(new StaffDetails(44 ,"N.NITHYA   ","PROFESSOR     ",32," MRS ","nithyangec@gmail.com   ","B.E , M.E , PH.D"));
	    cseStaff.add(new StaffDetails(103,"R.RIYAZKHAN","ASST.PROFESSOR",29," MR  ","riyazkhanngec@gmail.com","B.E , M.E"));
	    cseStaff.add(new StaffDetails(100,"K.RIYA     ","ASST.PROFESSOR",24,"MISS","riyangec@gmail.com     ","B.E , M.E"));
	    cseStaff.add(new StaffDetails(100,"R.ARUN     ","ASST.PROFESSOR",33,"MR  ","arunngec@gmail.com     ","B.E , M.E"));
	    cseStaff.add(new StaffDetails(100,"G.SHIVANI  ","ASST.PROFESSOR",23,"MISS","shivaningec@gmail.com  ","B.E , M.E"));
	    cseStaff.add(new StaffDetails( 44,"E.KINGSTON ","LAB ASSISTANT ",31," MR  ","kingstonngec@gmail.com ","B.E"));
	    cseStaff.add(new StaffDetails(122,"L.DURAI    ","LAB ASSISTANT ",28,"MR  ","duraingec@gmail.com    ","B.E"));
        
        List <String> cseSubjects = new ArrayList<String>();
        cseSubjects.add("PYTHON");
        cseSubjects.add("JAVA");
        cseSubjects.add("C");
        cseSubjects.add("Operating System");
        cseSubjects.add("MICROPROCESSOR");
        cseSubjects.add("NETWORKING");
        
        List <String> cseIv = new ArrayList<String>();
        cseIv.add("IT COMPANIES");
        cseIv.add("Computer parts manufacturing companies");
        cseIv.add("Networking companies");
        
        List <String> csePlacements = new ArrayList<String>();
        cseIv.add("ZOHO");
        cseIv.add("TCS");
        cseIv.add("CTS");
        cseIv.add("AMD");
        cseIv.add("NVIDIA");

        List<StaffDetails> eeeStaff=new ArrayList<StaffDetails>();
        eeeStaff.add(new StaffDetails(33 ,"A.LOKESH    ","HOD           ",43," MR  ","lokeshngec@gmail.com   ","B.E , M.E , PH.D"));
        eeeStaff.add(new StaffDetails(27 ,"B.ASHFAQ    ","PROFESSOR     ",39," MR  ","ashfaqngec@gmail.com   ","B.E , M.E , PH.D"));
        eeeStaff.add(new StaffDetails(50 ,"L.HARINI    ","PROFESSOR     ",37," MRS ","hariningec@gmail.com   ","B.E , M.E , PH.D"));
        eeeStaff.add(new StaffDetails(25 ,"G.PREM      ","PROFESSOR     ",41," MR  ","premngec@gmail.com     ","B.E , M.E , PH.D"));
        eeeStaff.add(new StaffDetails(61 ,"C.ESTER     ","PROFESSOR     ",32," MRS ","esterngec@gmail.com    ","B.E , M.E , PH.D"));
        eeeStaff.add(new StaffDetails(99 ,"T.HEMALATHA ","ASST.PROFESSOR",28," MRS ","hemalathangec@gmail.com","B.E , M.E"));
        eeeStaff.add(new StaffDetails(101,"C.HARISH    ","ASST.PROFESSOR",26,"MR  ","harishngec@gmail.com   ","B.E , M.E"));
        eeeStaff.add(new StaffDetails(116,"R.HEMA      ","ASST.PROFESSOR",23,"MISS","hemangec@gmail.com     ","B.E , M.E"));
        eeeStaff.add(new StaffDetails(120,"N.NITHISH   ","ASST.PROFESSOR",23,"MR  ","nithishngec@gmail.com  ","B.E , M.E"));
        eeeStaff.add(new StaffDetails(150,"H.AUSTIN    ","LAB ASSISTANT ",30,"MR  ","austinngec@gmail.com   ","B.E"));
        eeeStaff.add(new StaffDetails(133,"T.BALA      ","LAB ASSISTANT ",29,"MR  ","balangec@gmail.com     ","B.E"));
        
        List <String> eeeSubjects = new ArrayList<String>();
        eeeSubjects.add("Electrical Machines-I");
        eeeSubjects.add("Electric Circuit Analysis");
        eeeSubjects.add("Transducers and Sensors");
        eeeSubjects.add("Power Electronics");
        eeeSubjects.add("Industrial Management");

        List <String> eeeIv = new ArrayList<String>();
        eeeIv.add("Bharat Heavy Electrical Limited(BHEL)");
        eeeIv.add("Siemens Limited");
        eeeIv.add("Tata Consultancy");
        eeeIv.add("Bajaj International");

        List <String> eeePlacements = new ArrayList<String>();
        eeePlacements.add("BHEL");
        eeePlacements.add("Dev Denso Power Ltd.");
        eeePlacements.add("Electrotherm India Limited");
        eeePlacements.add("Tata");
        
        List<StaffDetails> mechanicalStaff=new  ArrayList<StaffDetails>();
        mechanicalStaff.add(new StaffDetails(29 ,"D.ARUNACHALAM","HOD           ",48," MR  ","arunachalamngec@gmail.com","B.E , M.E , PH.D"));
        mechanicalStaff.add(new StaffDetails(38 ,"A.SARAVANAN  ","PROFESSOR     ",37," MR  ","saravananngec@gmail.com  ","B.E , M.E , PH.D"));
        mechanicalStaff.add(new StaffDetails(59 ,"D.LOKESHWARAN","PROFESSOR     ",38," MR  ","lokeshngec@gmail.com     ","B.E , M.E , PH.D"));
        mechanicalStaff.add(new StaffDetails(98 ,"E.MARK       ","PROFESSOR     ",41," MR  ","markngec@gmail.com       ","B.E , M.E , PH.D"));
        mechanicalStaff.add(new StaffDetails(63 ,"O.AUGUSTINE  ","PROFESSOR     ",32," MR  ","augustinengec@gmail.com  ","B.E , M.E , PH.D"));
        mechanicalStaff.add(new StaffDetails(88 ,"T.DHEVAN     ","ASST.PROFESSOR",29," MR  ","dhevanngec@gmail.com     ","B.E , M.E"));
        mechanicalStaff.add(new StaffDetails(102,"P.RIYAZ      ","ASST.PROFESSOR",25,"MR  ","riyazngec@gmail.com      ","B.E , M.E"));
        mechanicalStaff.add(new StaffDetails(117,"T.SIVA       ","ASST.PROFESSOR",24,"MR  ","sivangec@gmail.com       ","B.E , M.E"));
        mechanicalStaff.add(new StaffDetails(128,"N.EMANUEL    ","ASST.PROFESSOR",23,"MR  ","emanuelngec@gmail.com    ","B.E , M.E"));
        mechanicalStaff.add(new StaffDetails(153,"L.IRFAN      ","LAB ASSISTANT ",32,"MR  ","irfanngec@gmail.com      ","B.E"));
        mechanicalStaff.add(new StaffDetails(139,"T.BALA       ","LAB ASSISTANT ",28,"MR  ","balangec@gmail.com       ","B.E"));
        
        List <String> mechSubjects = new ArrayList<String>();
        mechSubjects.add("THERMODYNAMICS");
        mechSubjects.add("FLUID MECHANICS");
        mechSubjects.add("SOLID MECHANICS");
        mechSubjects.add("ENGINEERING MATERIALS");

        List <String> mechIv = new ArrayList<String>();
        mechIv.add("Ashok Leyland");
        mechIv.add("Godrej");
        mechIv.add("L & T");
        mechIv.add("THERMAX");

        List <String> mechPlacements = new ArrayList<String>();
        mechPlacements.add("Kirloskar");
        mechPlacements.add("General Motors");
        mechPlacements.add("Thyssen Krupp");
        mechPlacements.add("L & T");
        mechPlacements.add("Tata group");
  
        List<StaffDetails> civilStaff=new ArrayList<StaffDetails>();
        civilStaff.add(new StaffDetails( 31,"D.DHEVANKUMAR","HOD           ",47," MR ","dhevankumarngec@gmail.com","B.E , M.E , PH.D"));
        civilStaff.add(new StaffDetails( 55,"L.NISHANTH   ","PROFESSOR     ",40," MR ","nishanthngec@gmail.com   ","B.E , M.E , PH.D"));
        civilStaff.add(new StaffDetails( 66,"A.SUNIL      ","PROFESSOR     ",35," MR ","sunilngec@gmail.com      ","B.E , M.E"));
        civilStaff.add(new StaffDetails( 65,"B.LOKESH     ","PROFESSOR     ",38," MR ","lokeshngec@gmail.com     ","B.E , M.E"));
        civilStaff.add(new StaffDetails( 71,"E.WINSTON    ","PROFESSOR     ",31," MR ","winstonngec@gmail.com    ","B.E , M.E"));
        civilStaff.add(new StaffDetails( 59,"R.ARAVIND    ","ASST.PROFESSOR",30," MR ","aravindngec@gmail.com    ","B.E , M.E"));
        civilStaff.add(new StaffDetails( 93,"A.ANIRUDH    ","ASST.PROFESSOR",29," MR ","anirudhngec@gmail.com    ","B.E , M.E"));
        civilStaff.add(new StaffDetails(131,"J.KISHORE    ","ASST.PROFESSOR",24,"MR ","kishorengec@gmail.com    ","B.E , M.E"));
        civilStaff.add(new StaffDetails(125,"K.ARUN       ","ASST.PROFESSOR",27,"MR ","arunngec@gmail.com       ","B.E , M.E"));
        civilStaff.add(new StaffDetails(141,"L.BALA       ","LAB ASSISTANT ",30,"MR ","balangec@gmail.com       ","B.E"));
        civilStaff.add(new StaffDetails(142,"G.NAVEEN     ","LAB ASSISTANT ",22,"MR ","naveenngec@gmail.com     ","B.E"));
        
        List <String> civilSubjects=new ArrayList<String >();
        civilSubjects.add("Structural Analysis");
        civilSubjects.add("Surveying");
        civilSubjects.add("Transportation Engineering");
        civilSubjects.add("It & Cad");
        civilSubjects.add("Design of Steel Structures");
        
        List <String> civilIv=new ArrayList<String >();
        civilIv.add("NTPC");
        civilIv.add("CIL");
        civilIv.add("IOCL");
        civilIv.add("SAIL");
        civilIv.add("NTPC");
  
        List <String> civilPlacements=new ArrayList<String >();
        civilPlacements.add("DLF Limited");
        civilPlacements.add("Tata projects");
        civilPlacements.add("RITES");
        civilPlacements.add("ESSAR group");
        civilPlacements.add("IRCON International Ltd");

        System.out.println("ENTER YOUR DEPARTMENT :  (ece ,cse , eee , mech , civil)");        
        Scanner input=new Scanner(System.in);
        String department=input.next();
        while(!"ece".equalsIgnoreCase(department) && !"cse".equalsIgnoreCase(department) && !"mech".equalsIgnoreCase(department) && !"eee".equalsIgnoreCase(department) && !"civil".equalsIgnoreCase(department)) {
				System.out.println("CHECK THE SPELLING YOUVE ENTERED. TRY AGAIN");
				 System.out.println("SELECT YOUR DEPARTMENT");
			        department=input.next();
			}
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("WELCOME TO "+department.toUpperCase()+" DEPARTMENT");
        System.out.println("-----------------------------------");
        System.out.println("STAFF DETAILS : ");
        System.out.println();
        System.out.println("|STAFF ID|    | STAFF NAME |   |DESIGNATION|      |AGE|           |STAFF MAIL ID|           |QUALIFICAION|");
        System.out.println("----------------------------------------------------------------------------------------------------------");

        switch(department.toLowerCase()) {
        case "ece" :
        	Collections.sort(eceStaff, new MySort());
              for(StaffDetails details:eceStaff) {
            		System.out.println(details);}
                    System.out.println();
              System.out.println("IN "+department.toUpperCase()+" DEPARTMENT YOU WILL LEARN:");
              System.out.println("-----------------------------------");
              for(String subjects:eceSubjects) {
                    System.out.println(subjects);}
   	                System.out.println("----------------------");
   	          System.out.println("INDUSTRIAL VISITS : ");
   	    	  System.out.println("---------------------------------------------------");
   	          for(String iv:eceIv) {
                    System.out.println(iv);}
     	            System.out.println("----------------------");
              System.out.println("PLACEMENT PROVIDERS : ");
       	      System.out.println("---------------------");
     	   	  for(String placements:ecePlacements) {
     	            System.out.println(placements);}
     	            System.out.println("----------------------");
           	break;
        case "cse" :
        	Collections.sort(cseStaff, new MySort());
        	for(StaffDetails details:cseStaff) {
               System.out.println(details);}
        	System.out.println();
        	  System.out.println("IN "+department.toUpperCase()+" DEPARTMENT YOU WILL LEARN:");
              System.out.println("-----------------------------------");
              for(String subjects:cseSubjects) {
                    System.out.println(subjects);}
   	                System.out.println("----------------------");
   	          System.out.println("INDUSTRIAL VISITS : ");
   	    	  System.out.println("---------------------------------------------------");
   	          for(String iv:cseIv) {
                    System.out.println(iv);}
     	            System.out.println("----------------------");
              System.out.println("PLACEMENT PROVIDERS : ");
       	      System.out.println("---------------------");
     	   	  for(String placements:csePlacements) {
     	            System.out.println(placements);}
     	            System.out.println("----------------------");
           	break;
        case "eee" :
        	Collections.sort(eeeStaff, new MySort());
        	for(StaffDetails details:eeeStaff) {
            System.out.println(details);
        	}
        	System.out.println();
      	  System.out.println("IN "+department.toUpperCase()+" DEPARTMENT YOU WILL LEARN:");
          System.out.println("-----------------------------------");
          for(String subjects:eeeSubjects) {
                System.out.println(subjects);}
	            System.out.println("----------------------");
	      System.out.println("INDUSTRIAL VISITS : ");
	      System.out.println("---------------------------------------------------");
          for(String iv:eeeIv) {
                System.out.println(iv);}
 	            System.out.println("----------------------");
          System.out.println("PLACEMENT PROVIDERS : ");
   	      System.out.println("---------------------");
 	   	  for(String placements:eeePlacements) {
 	            System.out.println(placements);}
 	            System.out.println("----------------------");
          break;
        case "mech" :
        	Collections.sort(mechanicalStaff, new MySort());
        	for(StaffDetails details:mechanicalStaff) {
        	System.out.println(details);	
        	}
        	System.out.println();
          System.out.println("IN "+department.toUpperCase()+" DEPARTMENT YOU WILL LEARN:");
          System.out.println("-----------------------------------");
          for(String subjects:mechSubjects) {
        	  System.out.println(subjects);}
    	      System.out.println("----------------------");
          System.out.println("INDUSTRIAL VISITS : ");
  	      System.out.println("---------------------------------------------------");
  	      for(String iv:mechIv) {
              System.out.println(iv);}
  	          System.out.println("----------------------");
          System.out.println("PLACEMENT PROVIDERS : ");
          System.out.println("---------------------");
   	   	  for(String placements:mechPlacements) {
   	   		  System.out.println(placements);}
     	      System.out.println("----------------------");
        	break;
        case "civil" :
        	Collections.sort(civilStaff, new MySort());
        	for(StaffDetails details:civilStaff) {
        		System.out.println(details);
        	}
        	System.out.println();
        	  System.out.println("IN "+department.toUpperCase()+" DEPARTMENT YOU WILL LEARN:");
              System.out.println("-----------------------------------");
              for(String subjects:civilSubjects) {
                    System.out.println(subjects);}
    	            System.out.println("----------------------");
    	      System.out.println("INDUSTRIAL VISITS : ");
    	      System.out.println("---------------------------------------------------");
              for(String iv:civilIv) {
                    System.out.println(iv);}
     	            System.out.println("----------------------");
              System.out.println("PLACEMENT PROVIDERS : ");
       	      System.out.println("---------------------");
     	   	  for(String placements:civilPlacements) {
     	            System.out.println(placements);}
     	            System.out.println("----------------------");
              break;
        default :
        	System.out.println("CHECK THE DEPARTMENT YOUVE ENTERED");
        }
		
			}
}

class MySort implements Comparator<StaffDetails>{
public int compare(StaffDetails o1,StaffDetails o2) {
	return (int) (o1.getId()-o2.getId());
}
}
