package com.javaproject;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
public class CollegeMain{

	public static void main(String[] args) throws FileNotFoundException, IOException {
	while(true) {
	List<String> userChoices=new ArrayList<String>();
	userChoices.add("1.STUDENT ADMISSION");
	userChoices.add("2.CUT-OFF CALCULATION");
	userChoices.add("3.ADMISSION DETAILS");
	userChoices.add("4.BALANCE FEES PAYMENT");
    userChoices.add("5.STAFF & DEPARTMENT DETAILS");
    userChoices.add("6.CLASS SCHEDULE & SECTION ALLOTMENT");
	userChoices.add("7.COLLEGE BUS");
	userChoices.add("8.ALL STUDENTS ADMISSION IDS");
	userChoices.add("9.EXIT");
	System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	System.out.println("___________________________________________________________________________________       NEXT GEN ENGINEERING COLLEGE      ____________________________________________________________________________________________");
	System.out.println("\n                                                                                        ~SHAPING YOUNG MINDS SINCE 2000~                                                                                              ");
	System.out.println("________________________________________________________________________________________________________________________________________________________________________________________________________________________");
	System.out.println("|HOME|                               |LOGIN|                                |EVENTS|                                        |AWARDS|                                              |ABOUT|                               ");
	System.out.println("________________________________________________________________________________________________________________________________________________________________________________________________________________________");
	for(String b:userChoices) {
		System.out.println(b);
	}
	
    Scanner input=new Scanner(System.in);
	int userInput=input.nextInt();
	switch (userInput) {
	case 1 :
	    StudentAdmission newStudent=new StudentAdmission();
	    newStudent.displayInfo();
        break;
	case 2 :
	    CutOffCalculation b=new CutOffCalculation();
	    b.calculate();
        break;
	case 3 :
		AdmissionDetails details= new AdmissionDetails();
		details.admission();
        break;
	case 4 :
		AdmissionDetails balanceFees= new AdmissionDetails();
		balanceFees.balanceFeesPayment();
        break;
	case 5 :
		StaffAndDepartmentData data=new StaffAndDepartmentData();
		data.departmentData();
        break;
	case 6 :
		SectionAndSchedule allotment=new SectionAndSchedule();
		allotment.sectionAndTimetable();
        break;
	case 7 :
		BusList bus=new BusList();
		bus.busMethod();
        break;
	case 8 :
		AdmissionDetails studentIds= new AdmissionDetails();
		studentIds.allStudentids();
        break;
	case 9 :
	    System.out.println("EXITED");
	    System.out.println("+++++++++++++++++++=");
	    System.exit(0);
	default :
		System.out.println("ENTER A VALID CHOICE");
		
	}
	}
 }

}