package com.javaproject;

import java.util.ArrayList;
import java.util.Scanner;

public class SectionAndSchedule {
    String section;

	public void sectionAndTimetable() {
		ArrayList <Schedule> schedule=new ArrayList<Schedule> ();
	    Scanner input=new Scanner(System.in);
    	System.out.println("ENTER YOUR NAME");
    	String studentName= input.next();
    	String str=studentName.replaceAll("\\s", "");
		char c[]=str.toCharArray();
		System.out.println();
		System.out.println("SECTION ALLOTMENT");
		System.out.println("-----------------");
		System.out.println("IF YOUR NAME IS BETWEEN A TO N,GO TO 'A' SECTION \nIF IT'S BETWEEN O TO Z ,GO TO 'B' SECTION.");
		System.out.println();
		for(int i=0;i<c.length;i++) {
			if(c[i]!=' ' &&(i==0||c[i-1]==' ')) {
				String firstLetter=String.valueOf(c[i]);
				if(firstLetter.equalsIgnoreCase("a") ||firstLetter.equalsIgnoreCase("b") || firstLetter.equalsIgnoreCase("c") || firstLetter.equalsIgnoreCase("d") ||firstLetter.equalsIgnoreCase("e") ||firstLetter.equalsIgnoreCase("f") ||firstLetter.equalsIgnoreCase("g") ||firstLetter.equalsIgnoreCase("h") ||firstLetter.equalsIgnoreCase("h") ||firstLetter.equalsIgnoreCase("i") ||firstLetter.equalsIgnoreCase("j") ||firstLetter.equalsIgnoreCase("k") ||firstLetter.equalsIgnoreCase("l") ||firstLetter.equalsIgnoreCase("m") ||firstLetter.equalsIgnoreCase("n") )
				{   
					section="A";
					System.out.println("SINCE YOUR NAME STARTS WITH '"+firstLetter.toUpperCase()+"', PLEASE GO TO "+section+"  SECTION");

				}
				else {
					section="B";
					System.out.println("SINCE YOUR NAME STARTS WITH '"+firstLetter.toUpperCase()+"', PLEASE GO TO "+section+"  SECTION");
				} 
				}
			}
		System.out.println();
		/*System.out.println("ENTER YOUR SECTION : ");
		section=input.next();*/
		System.out.println("YOUR 1st SEMESTER "+section.toUpperCase()+" SECTION TIME TABLE~");
		System.out.println();
                   System.out.println("------------------------------------------------------------------------------------------------------------------");
		           System.out.println("DAY       | 1st Period     |   2nd Period  |   3rd Period    | 4th Period   |  5th Period  | 6th Period   |");
		           System.out.println("------------------------------------------------------------------------------------------------------------------");
		switch(section.toUpperCase()) {
		case "A" :
			schedule.add(new Schedule("|MONDAY   |","ENGLISH       |","ENGG MATHS I |","ENGG CHEMISTRY|","ENGG PHYSICS  |","PYTHON      |","ENGG GRAPHICS|"));
			schedule.add(new Schedule("|TUESDAY  |","ENGLISH       |","ENGG GRAPHICS|","ENGG PHYSICS  |","ENGG CHEMISTRY|","PYTHON      |","ENGLISH      |"));
			schedule.add(new Schedule("|WEDNESDAY|","ENGG GRAPHICS |","ENGG PHYSICS |","ENGG CHEMISTRY|","ENGG MATHS I  |","ENGLISH     |","PYTHON       |"));
			schedule.add(new Schedule("|THURSDAY |","ENGG CHEMISTRY|","ENGG MATHS I |","ENGG GRAPHICS |","ENGG PHYSICS  |","ENGLISH     |","PYTHON       |"));
			schedule.add(new Schedule("|FRIDAY   |","ENGG PHYSICS  |","ENGLISH      |","PYTHON        |","ENGG CHEMISTRY|","ENGG MATHS I|","ENGG GRAPHICS|"));
			for(Schedule aSection : schedule) {
				System.out.println(aSection);
			}
		       System.out.println("-------------------------------------------------------------------------------------------------------------------");
			break;
		case "B" :
			schedule.add(new Schedule("|MONDAY   |","ENGG PHYSICS  |","ENGG MATHS I |","ENGG CHEMISTRY|","ENGLISH       |","PYTHON        |","ENGG GRAPHICS|"));
			schedule.add(new Schedule("|TUESDAY  |","ENGLISH       |","ENGG GRAPHICS|","ENGG PHYSICS  |","ENGG CHEMISTRY|","PYTHON        |","ENGLISH      |"));
			schedule.add(new Schedule("|WEDNESDAY|","ENGG GRAPHICS |","ENGG PHYSICS |","ENGG CHEMISTRY|","ENGG MATHS I  |","ENGLISH       |","PYTHON       |"));
			schedule.add(new Schedule("|THURSDAY |","ENGG CHEMISTRY|","ENGG MATHS I |","ENGG GRAPHICS |","ENGG PHYSICS  |","ENGLISH       |","PYTHON       |"));
			schedule.add(new Schedule("|FRIDAY   |","ENGLISH       |","ENGG MATHS I |","ENGG CHEMISTRY|","ENGG PHYSICS  |","ENGG GRAPHICS |","PYTHON       |"));

			for(Schedule bSection : schedule) {
				System.out.println(bSection);
			}
	           System.out.println("---------------------------------------------------------------------------------------------------------------------");

            break;
        default :
        	System.out.println("INVALID CHOICE");
			
		}
		}
}
