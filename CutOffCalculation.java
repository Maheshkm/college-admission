package com.javaproject;
import java.util.*;
import java.lang.*;
public class CutOffCalculation {
	double cutOffValue;
	double physics;
	double chemistry;
	double maths;
    double fullMark;
	Scanner input=new Scanner(System.in);
	
	public void calculate() {
			System.out.println("CUT OFF MARK CALCULATION");
			System.out.println("------------------------");
			System.out.println("ENTER YOUR PHYSICS MARK");
		    physics=input.nextDouble();
			System.out.println("ENTER YOUR CHEMISTRY MARK");
		    chemistry=input.nextDouble();
			System.out.println("ENTER YOUR MATHS MARK");
		    maths=input.nextDouble();
			System.out.println("ENTER THE FULL MARKS OF THE SUBJECTS : (i.e 200 OR 100)");
		    fullMark=input.nextDouble();
		    double cutoff=(((physics/fullMark)*50)+((chemistry/fullMark)*50)+((maths/fullMark)*100));
		    cutOffValue=Math.round(cutoff);
			System.out.println("YOUR CUT OFF MARK IS :"+cutOffValue);
	}
	public void cutOff(double cutOffMark) {
		System.out.println();
		System.out.println("OUR COLLEGE MAINTAINS A CUT-OFF MARK FOR JOINING A DEPARTMENT");
		 System.out.println("-------------------------------------");
		 System.out.println("THE CUT-OFF MARK FOR : ");
         System.out.println("-----------------------");
         HashMap<String,Integer> departments=new HashMap<String,Integer>();
         departments.put("Computer Science Engineering",160);
         departments.put("Electronics Communication Engineering", 150);
         departments.put("Electrical & Electronics Engineering", 145);
         departments.put("Mechanical Engineering", 140);
         departments.put("Civil Engineering", 140);
         Iterator<Map.Entry<String, Integer>> depts=departments.entrySet().iterator();
         while(depts.hasNext()) {
        	 Map.Entry<String, Integer> pair=depts.next();
        	 System.out.format("%s = %d%n",pair.getKey(),pair.getValue());
         }
		 System.out.println("-------------------------------------");
		 if (cutOffMark>=160) {
         System.out.println("Since your cut-off mark is "+cutOffMark);
         System.out.println("You can join any department");
		 }
		 else if(cutOffMark>=150) {
	         System.out.println("Since your cut-off mark is "+cutOffMark);
			 System.out.println("you cannot join CSE department. You can join any remaining departments.");
		 }
		 else if(cutOffMark>=145) {
	         System.out.println("Since your cut-off mark is "+cutOffMark);
			 System.out.println("you can only join EEE , MECH & CIVIL department");
		 }
		 else if(cutOffMark>=140){
	         System.out.println("Since your cut-off mark is "+cutOffMark);
			 System.out.println("you can only join MECH & CIVIL department");
		 }
		 else {
	         System.out.println("Since your cut-off mark is "+cutOffMark);
			 System.out.println("you cannot join any department");
		 }
		}
}