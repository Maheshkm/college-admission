package com.javaproject;

public class StaffDetails {
	private int id;
	private String designation;
	private String name;
	private int age;
	private String honorifics;
	private String emailId;
	private String qualifications;
    
	public StaffDetails(int id, String name,String designation, int age, String honorifics, String emailId,String qualifications) {
		super();
		this.id = id;
		this.name = name;
		this.designation=designation;
		this.age = age;
		this.honorifics =honorifics;
		this.emailId=emailId;
		this.qualifications=qualifications;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getHonorifics() {
		return honorifics;
	}

	public void setHonorifics(String honorifics) {
		this.honorifics = honorifics;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setMarriageStatus(String emailId) {
		this.emailId = emailId;
	}

	public String getQualifications() {
		return qualifications;
	}

	public void setQualifications(String qualifications) {
		this.qualifications = qualifications;
	}

	@Override
	public String toString() {
		return "    "+id+"      "+honorifics+""+name+"   "+designation+"     "+age+"         "+emailId+"       "+qualifications ;

	}

}
