package com.javaproject;
import java.util.ArrayList;
import java.util.Scanner;
public class Booking {
    String studentName;
    int busNo;     
    public void Bookings(){
   	  Scanner input=new Scanner(System.in);
   	  System.out.println("ENTER YOUR NAME:");
   	  studentName=input.nextLine();
   	  System.out.println("ENTER BUS NO:");
   	  busNo=input.nextInt();
   	
     }
     public boolean isAvailabe(ArrayList<Booking>bookings,ArrayList<Bus>buses) {
   	  int capacity=0;
   	  for(Bus bus:buses) {
			if(bus.getBusNo()==busNo)
   			  capacity=bus.getCapacity();
   	  }
   	  int booked=0;
   	  for (Booking b: bookings) {
   		  if(b.busNo==busNo) {
   			  booked++;
   		  }
   	  }
   	  return booked<capacity?true:false;
     }

}
