package com.javaproject;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
public class StudentAdmission extends FeesMethods {
	 Scanner input=new Scanner(System.in);
	 String firstGraduate;
	 int firstGraduateDiscount;

	 ArrayList<Student> newStudent=new ArrayList<Student>();

	 public void displayInfo() throws FileNotFoundException, IOException {
		 
			System.out.println("ENTER YOUR NAME:");
			String name=input.nextLine();
			
			System.out.println("ENTER YOUR AGE : (limit 17 to 30)");                      //LIMIT 17 to 30
		    int age=input.nextInt();
		    while(age<17 || age>30) {				
		    System.out.println("AGE LIMIT FOR OUR COLLEGE IS 17 to 30");
			System.out.println("YOU ARE NOT ELIGIBLE TO JOIN OUR COLLEGE");
			System.exit(0);
            }
			
			System.out.println("ENTER YOUR GENDER : (Inputs : male / female / others)");
			String gender=input.next();
			while(!"male".equalsIgnoreCase(gender) && !"female".equalsIgnoreCase(gender) & !"others".equalsIgnoreCase(gender))  //&& gender!="female" && gender!="others") 
			{
			System.out.println("check the spelling");
			System.out.println("ENTER YOUR GENDER");
			gender=input.next();
		    }
			
			System.out.println("ENTER YOUR 10TH MARK : (limit 500)");                          //Limit 500
		    int xmark=input.nextInt();
		    while(xmark>500) {					
		    System.out.println("Value exceeds full mark");
			System.out.println("Check value you've entered and enter again");
			System.out.println("ENTER YOUR 10TH MARK");
			xmark=input.nextInt();}
		    
		    System.out.println("ENTER YOUR 12TH MARK : (limit 1200)");                           //Limit 600 to 1200
		    int xiimark=input.nextInt();
		    while(xiimark>1200) {
				System.out.println("Check value you've entered");
			    System.out.println("ENTER YOUR 12TH MARK");
				xiimark=input.nextInt();
		    }
		    
		    System.out.println("ENTER YOUR CASTE: (Inputs : oc / bc / obc / mbc / sc / st)");
		    String caste=input.next();
		    while(!"oc".equalsIgnoreCase(caste) && !"bc".equalsIgnoreCase(caste) && !"obc".equalsIgnoreCase(caste) && !"mbc".equalsIgnoreCase(caste) && !"sc".equalsIgnoreCase(caste) && !"st".equalsIgnoreCase(caste)) {
				System.out.println("WRONG VALUE ENTERED. ENTER AGAIN ");
			    System.out.println("ENTER YOUR CASTE:");
			    caste=input.next();
		    }
		    
		    System.out.println("ENTER YOUR CUT-OFF MARK :  (limit 200)");                            //Limit 200
	        double cutOffMark=input.nextDouble();
	        while(cutOffMark>200)
	        {     
				System.out.println("Check value you've entered");
	        	 System.out.println("ENTER YOUR CUT-OFF MARK");                            
	  	        cutOffMark=input.nextDouble();	        }			

	        CutOffCalculation a=new CutOffCalculation();
	        a.cutOff(cutOffMark); 
	        System.out.println("You can join the department you like through management quota eventhough your cut-off is not sufficient");
	        System.out.println();
	        System.out.println("COUNSELLING OR MANAGEMENT?");
	        String counsellingOrManage=input.next();
		    while(!"counselling".equalsIgnoreCase(counsellingOrManage)&& !"management".equalsIgnoreCase(counsellingOrManage)) {
					System.out.println("Check the spelling you've entered and enter again");
			        System.out.println("COUNSELLING OR MANAGEMENT?");
			        counsellingOrManage=input.next();
		    }
	        
	        System.out.println("SELECT YOUR DEPARTMENT :  (Inputs : ece / cse / mech / eee / civil)");
	        String department=input.next();
	        while(!"ece".equalsIgnoreCase(department) && !"cse".equalsIgnoreCase(department) && !"mech".equalsIgnoreCase(department) && !"eee".equalsIgnoreCase(department) && !"civil".equalsIgnoreCase(department)) {
					System.out.println("CHECK THE SPELLING YOUVE ENTERED. TRY AGAIN");
					 System.out.println("SELECT YOUR DEPARTMENT");
				        department=input.next();
				}
		
		    System.out.println("DO YOU HAVE YOUR FIRST GRADUATION CERTIFICATE ?  (Inputs : yes / no)");
		    firstGraduate=input.next();
	        while(!"yes".equalsIgnoreCase(firstGraduate) && !"no".equalsIgnoreCase(firstGraduate)){
		   		   System.out.println("CHECK THE SPELLING YOUVE ENTERED. TRY AGAIN");
			       System.out.println("DO YOU HAVE YOUR FIRST GRADUATION CERTIFICATE ?");
			       firstGraduate=input.next();
		    }
		      
	        switch(firstGraduate.toLowerCase()) {
		    case "yes" :
		    	   System.out.println("A SCHOLARSHIP OF RS.25000 IS PROVIDED FOR FIRST GRADUATE STUDENTS.");
		    	   System.out.println();
		    	   firstGraduate="yes";
		    	   firstGraduateDiscount=25000;
		    	   break;
            case "no" :
	 	    	   System.out.println("CONTACT OUR MANAGEMENT TO ACQUIRE DETAILS ABOUT FIRST GRADUATION CERTIFICATE APPLYING PROCESS");
		    	   firstGraduate="nil";
		    	   firstGraduateDiscount=0;
		       }

            //fees calculation
	        if(caste.equalsIgnoreCase("sc" )|| caste.equalsIgnoreCase("st"))
	        {
		           System.out.println("OUR COLLEGE OFFERS SCHOLARSHIP TO "+caste+" CASTE STUDENTS SO YOU DON'T HAVE TO PAY TUTION FEES");
		           System.out.println();
		           System.out.println("you have to pay dayscholar/hostel fees only");
	        }
	        else {     
		    //for counselling
	        if(counsellingOrManage.equalsIgnoreCase("counselling"))      
	        {       
	        		counselling(firstGraduateDiscount);
	        }
	        else {    
	        //for management 
    	      	System.out.println("WELCOME TO MANAGEMENT QUOTA");
                System.out.println("---------------------------");
	       		management(firstGraduateDiscount);	        
	       }
	      }
	 
	 
	       System.out.println("ARE YOU A DAYSCHOLAR OR HOSTELER?  (Inputs : dayscholar or hostel");
	       String dayOrHostel=input.next();
	       while(!"DAYSCHOLAR".equalsIgnoreCase(dayOrHostel) && !"HOSTELER".equalsIgnoreCase(dayOrHostel)) {
				System.out.println("CHECK THE SPELLING YOUVE ENTERED. TRY AGAIN");
		        System.out.println("ARE YOU A DAYSCHOLAR OR HOSTELER?");
		        dayOrHostel=input.next();
		   }
	        
  //Dayscholar or hosteler
	       if(dayOrHostel.equalsIgnoreCase("dayscholar"))
	       {
	    	  dayScholar();
	    	  dayOrHostel="BUS FEES  ";
	       }
	       else if(dayOrHostel.equalsIgnoreCase("hosteler")) {
	    	  hosteler();  
	    	  dayOrHostel="MESS FEES ";
	       } 
	       System.out.println("-------------------------------------------------------------");
	       System.out.println("CONGRATULATIONS "+name.toUpperCase()+" ON JOINING OUR COLLEGE");
	       System.out.println("-------------------------------------------------------------");
	       System.out.println("THANK YOU FOR JOINING "+department.toUpperCase()+" DEPARTMENT");
	       System.out.println();
	       
	       //FIRST GRADUATE
	       
	      
	       //STUDENT ID GENERATION
	  	   int studentId=0;
	   	   int limit=10;
	       int id;
		   for (int roll = 1 ; roll < limit ; roll++) 
           {
			id = (int)(Math.random()* 50 +55);
			studentId+=id;
		    }	
		    studentId+=500000;
		    System.out.println();
		    System.out.println("YOUR ADMISSION ID IS : "+ studentId);
			System.out.println("-----------------------");
	        System.out.println("YOU CAN CHECK YOUR ADMISSION DETAILS AT 'ADMISSION DETAILS' SECTION USING YOUR ADMISSION ID.");
            
	        newStudent.add(new Student (name,age,gender,department,dayOrHostel,fees,busOrHostelFees,messFees,hostelFees,studentId,totalFees,balanceTutionFees,balanceMessFees,balanceHostelFees));
	        for(Student student1: newStudent) {
	        	student1.reciept();
	        	System.out.println("please screenshot your reciept for future reference");
	        	System.out.println();
	        }
	        
	        Student newStudent =new Student();
	        
	        //FILE CREATION & WRITER
	        String userpath = "C:\\Users\\" + System.getProperty("user.name") + "\\Desktop\\StudentAdmission\\";
	        File createdir = new File (userpath);
	        if(!createdir.exists()) {
	        	createdir.mkdir();
	        }
	        File studentFile = new File(userpath +studentId+ ".txt");
	        studentFile.createNewFile();
			FileWriter myWriter =new FileWriter(userpath +studentId+ ".txt");
			
			//Storing all students ids and their names
		     File allStudentId = new File(userpath +"allstudentId"+ ".txt");
				if(!allStudentId.exists()) {
					allStudentId.createNewFile();
				}
				FileWriter write =new FileWriter(allStudentId , true);
				BufferedWriter buffer=new BufferedWriter(write);
				buffer.write("STUDENT ID : " +studentId+"\tSTUDENT NAME : "+name+"\tDATE OF JOINING : "+newStudent.date);
				buffer.newLine();
				buffer.close();

			if(dayOrHostel.equalsIgnoreCase("dayscholar"))
			{
			dayOrHostel="DAYSCHOLAR";
			myWriter.write("\nSTUDENT NAME : " +name.toUpperCase()+ "\n\nAGE : " +age+ "\n\nGENDER : " +gender+ "\n\nCASTE : "+caste+"\n\nCut OFF MARK : "+cutOffMark+"\n\nDEPARTMENT : "+department
					+"\n\nMETHOD OF ADMISSION : "+counsellingOrManage+"\n\nDAY SCHOLAR OR HOSTELER : "+dayOrHostel+"\n\nTUTION FEES PAID : "+fees+"\n\nBALANCE TUTION FEES :"+balanceTutionFees+
					"\n\nBUS FEES PAID : "+busOrHostelFees+"\n\nBALANCE BUS FEES : "+balanceBusOrHostelFees+"\n\nMESS FEES PAID : "+messFees+"\n\nBALANCE MESS FEES : "+balanceMessFees+
					"\n\nTOTAL FEES PAID : "+totalFees+"\n\nFIRST GRADUATION CERTIFICATE : "+firstGraduate+"\n\nFIRST GRADUATE DISCOUNT : "+firstGraduateDiscount+"\n\nSTUDENT ID : "+studentId);
			}
			else {
				dayOrHostel="HOSTELER";
				myWriter.write("\nSTUDENT NAME : " +name.toUpperCase()+ "\n\nAGE : " +age+ "\n\nGENDER : " +gender+ "\n\nCASTE : "+caste+"\n\nCut OFF MARK : "+cutOffMark+"\n\nDEPARTMENT : "+department
						+"\n\nMETHOD OF ADMISSION : "+counsellingOrManage+"\n\nDAY SCHOLAR OR HOSTELER : "+dayOrHostel+"\n\nTUTION FEES PAID : "+fees+"\n\nBALANCE TUTION FEES : "+balanceTutionFees+
						"\n\nHOSTEL FEES PAID : "+busOrHostelFees+"\n\nBALANCE HOSTEL FEES : "+balanceBusOrHostelFees+"\n\nMESS FEES PAID : "+messFees+"\n\nBALANCE MESS FEES : "+balanceMessFees+
						"\n\nTOTAL FEES PAID : "+totalFees+"\n\nFIRST GRADUATION CERTIFICATE : "+firstGraduate+"\n\nFIRST GRADUATE DISCOUNT : "+firstGraduateDiscount+"\n\nSTUDENT ID : "+studentId);

			}
			myWriter.flush();
			myWriter.close();	
          
	 }

}

