package com.javaproject;
import java.util.ArrayList;
import java.util.Scanner;
public class BusList {
	public void busMethod()  {
		Booking booking=new Booking();

		ArrayList<Bus> tambaramBuses=new ArrayList<Bus>();
		ArrayList<Bus> kalpakkamBuses=new ArrayList<Bus>();
		ArrayList<Bus> velacheryBuses=new ArrayList<Bus>();
		ArrayList<Bus> vandalurBuses=new ArrayList<Bus>();
		ArrayList<Bus> broadwayBuses=new ArrayList<Bus>();
	
		ArrayList<Booking> bookings=new ArrayList<Booking>();
		ArrayList<String> boardingLocation=new ArrayList<String>();
		boardingLocation.add("TAMBARAM");
		boardingLocation.add("KALPAKKAM");
		boardingLocation.add("VELACHERY");
		boardingLocation.add("VANDALUR");
		boardingLocation.add("BROADWAY");

		tambaramBuses.add(new Bus(1,"PREM",30,"TAMBARAM","+91 9347761059"));
		tambaramBuses.add(new Bus(2,"DEEPAK",32,"TAMBARAM","+91 8874435544"));

		kalpakkamBuses.add(new Bus(3,"BALA",30,"KALPAKKAM","+91 8290406624"));
		kalpakkamBuses.add(new Bus(4,"ARUN",40,"KALPAKKAM","+91 9965431009"));

		velacheryBuses.add(new Bus(5,"LOKESH",50,"VELACHERY","+91 8425616666"));
		velacheryBuses.add(new Bus(6,"DURAI",35,"VELACHERY","+91 8533010884"));

		vandalurBuses.add(new Bus(7,"MALIK",30,"VANDALUR","+91 9345605556"));
		vandalurBuses.add(new Bus(8,"YUKESH",30,"VANDALUR","+91 7345525366"));

		broadwayBuses.add(new Bus(9,"PAUL",30,"BROADWAY","+91 8452170994"));
		broadwayBuses.add(new Bus(10,"PAUL",30,"BROADWAY","+91 9032448211"));

		Scanner input=new Scanner(System.in);
		System.out.println("WELCOME TO BUS INFORMATION SECTION");
		System.out.println("----------------------------------");
		System.out.println("AVAILABLE BOARDING POINTS:");
		System.out.println("--------------------------");
		
		for(String location:boardingLocation) {
			System.out.println(location);
		}
			System.out.println("Enter 1 to apply for college bus or 2 to cancel");
			int userinpt=input.nextInt();
			if(userinpt==1) {
				System.out.println("ENTER YOUR BOARDING POINT");
                String boardingPoint=input.next();
                
				switch(boardingPoint.toLowerCase()) {
				case "tambaram":
					System.out.println("AVAILABLE TAMBARAM BUSES");
					System.out.println("-------------------------");

					 for (Bus tambaram:tambaramBuses) {
					    	tambaram.displayBusInfo();
					    }
						booking.Bookings();
					 if(booking.isAvailabe(bookings,tambaramBuses)) {
							bookings.add(booking);
							System.out.println("Dear "+booking.studentName+" Your application is submitted for bus no:"+booking.busNo);
						}
						else
						{
							System.out.println("Sorry bus is full try another bus");
				     	}
				break;
				case "kalpakkam" :
					System.out.println("AVAILABLE KALPAKKAM BUSES");
					System.out.println("-------------------------");
					 for (Bus kalpakkam:kalpakkamBuses) {
						 kalpakkam.displayBusInfo();
					 }	
						booking.Bookings();

					 if(booking.isAvailabe(bookings,kalpakkamBuses)) {
							bookings.add(booking);
							System.out.println("Dear "+booking.studentName+" Your application is submitted for bus no:"+booking.busNo);
						}
						else
						{
							System.out.println("Sorry bus is full try another bus");
					}
					 break;
				case "velachery":
					System.out.println("AVAILABLE VELACHERY BUSES");
					System.out.println("-------------------------");

					 for (Bus velachery:velacheryBuses) {
						 velachery.displayBusInfo();
					 }
						booking.Bookings();

						if(booking.isAvailabe(bookings,velacheryBuses)) {
							bookings.add(booking);
							System.out.println("Dear "+booking.studentName+" Your application is submitted for bus no:"+booking.busNo);
						}
						else
						{
							System.out.println("Sorry bus is full try another bus");
					}
					 break;
				case "vandalur":
					System.out.println("AVAILABLE VANDALUR BUSES");
					System.out.println("-------------------------");

					 for (Bus vandalur:vandalurBuses) {
						 vandalur.displayBusInfo();
					 }
						booking.Bookings();

					 if(booking.isAvailabe(bookings,vandalurBuses)) {
							bookings.add(booking);
							System.out.println("Dear "+booking.studentName+" Your application is submitted for bus no:"+booking.busNo);
						}
						else
						{
							System.out.println("Sorry bus is full try another bus");
					}
					 break;
				case "broadway" :
					System.out.println("AVAILABLE BROADWAY BUSES");
					System.out.println("-------------------------");

					 for (Bus broadway:broadwayBuses) {
						 broadway.displayBusInfo();
					 }
						booking.Bookings();

					 if(booking.isAvailabe(bookings,broadwayBuses)) {
							bookings.add(booking);
							System.out.println("Dear "+booking.studentName+" Your application is submitted for bus no:"+booking.busNo);
						}
						else
						{
							System.out.println("Sorry bus is full try another bus");
					}break;
					

				default:
					System.out.println("CHECK THE BUS NAME YOUVE ENTERED");
			}
			}else {
				System.out.println("EXITED");
			}
			
		
		}

}
