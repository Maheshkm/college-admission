package com.javaproject;

public class Bus {
	int busNo;
    String driverName;
    int capacity;
    String boardingPoint;
    String phoneNumber;
    Bus(int no, String driverName,int capacity,String boardingPoint,String phoneNumber){
 	   this.busNo=no;
 	   this.driverName=driverName;
 	   this.capacity=capacity;
 	   this.boardingPoint=boardingPoint;
 	   this.phoneNumber=phoneNumber;
    }
	public int getBusNo() {
		return busNo;
	}

	public void setBusNo(int busNo) {
		this.busNo = busNo;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

    public String getBoardingPoint() {
		return boardingPoint;
	}

	public void setBoardingPoint(String boardingPoint) {
		this.boardingPoint = boardingPoint;
	}
 
	public void displayBusInfo() {
 	 System.out.println("BUS NO:"+busNo);
     System.out.println("DRIVER NAME:"+driverName);
     System.out.println("TOTAL CAPACITY:"+capacity);
     System.out.println("BOARDING POINT:"+boardingPoint);
     System.out.println("DRIVER CONTACT NUMBER:"+phoneNumber);
     System.out.println("------------------------------");
 }
    

}
