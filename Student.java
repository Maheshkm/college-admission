package com.javaproject;

import java.time.LocalDate;

public class Student {
 private String name;
 private int age;
 private String gender;
 private String department;
 private String dayOrHostel;
 private int fees;
 private int busOrHostelFees;
 private int messFees;
 private int totalFeesPaid;
 private int studentId;
 private int balanceTutionFees;
 private int balanceMessFees;
 private int balanceHostelFees;

LocalDate date= LocalDate.now();
  Student(){
  }
  Student(String name, int age, String gender, String department, String dayOrHostel,int fees, int busOrHostelFees, int messFees,  int hostelFees,int studentId,int totalFeesPaid, int balanceTutionFees, int balanceMessFees, int balanceHostelFees)
  {
	this.name=name;
	this.age=age;
	this.gender=gender;
	this.department=department;
	this.dayOrHostel=dayOrHostel;
	this.fees=fees;
	this.busOrHostelFees=busOrHostelFees;
	this.messFees=messFees;
	this.totalFeesPaid=totalFeesPaid;
	this.studentId=studentId;
	this.balanceTutionFees=balanceTutionFees;
	this.balanceMessFees=balanceMessFees;
	this.balanceHostelFees=balanceHostelFees;
}
	   public void reciept() {
			System.out.println("=========================================================");
			System.out.println("|_______________NEXT GEN ENGINEERING COLLEGE____________|");
			System.out.println("|                                                       |");
			System.out.println("|             ~SHAPING YOUNG MINDS SINCE 2000~          |");
			System.out.println("|                                                       |");
			System.out.println("|                                PHONE:+91 44322 23444  |");
			System.out.println("|                                                       |");
			System.out.println("|                                                       |");
			System.out.println("|                                                       |");
			System.out.println("|STUDENT NAME:"+name.toUpperCase()+"               COURSE:"+department.toUpperCase()+"         ");
			System.out.println("|                                                       |");
			System.out.println("|AGE:"+age+"                           GENDER:"+gender.toUpperCase()+"                   ");
			System.out.println("|                                                       |");
			System.out.println("|STUDENT ID:"+studentId+"                                      |");
			System.out.println("|                                                       |");
			System.out.println("|DATE OF PAYMENT: "+date+"                            |");
			System.out.println("|                                                       |");
			System.out.println("|_______________________________________________________|");
			System.out.println("| S.NO |           PARTICULARS           |   AMOUNT     |");
			System.out.println("|------|---------------------------------|--------------|");
			System.out.println("|1.    | TUTION FEES                     |"+fees+"         |");
			System.out.println("|2.    | "+dayOrHostel.toUpperCase()+"                      |"+busOrHostelFees+"         |    ");
			System.out.println("|3.    | MESS FEES                       |"+messFees+"         |     ");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|      |                                 |              |");
			System.out.println("|______|_________________________________|______________|");
			System.out.println("|                                  TOTAL |"+totalFeesPaid+" ");
			System.out.println("|                                        ---------------|");
			System.out.println("|                                                       |");
			System.out.println("|                                                       |");
			System.out.println("|                                                       |");
			System.out.println("|                                                       |");
			System.out.println("| SIGNATUERE OF                      SIGNATURE OF THE   |");
			System.out.println("|  PRINCIPAL                             STUDENT        |");
			System.out.println("|                                                       |");
			System.out.println("|                                                       |");
	        System.out.println("|_______________________________________________________|");
			System.out.println("=========================================================");
	}
		  public void print() {
			  System.out.println(date);
		  }		

}
